O jogo batalha naval possui dois players e dois mapas 13x13 contendo 12 embarcações de cada player. Quem destruir as 12 embarcações primeiro, ganha. 
Cada player possui 16 vidas:
- 6 de canoas, que precisam somente serem atingidas uma vez para serem destruídas.
- 8 de submarinos, sendo q cada casa do submarino precisa ser atingida duas vezes para ser destruída e perder uma vida.
- 2 de porta-aviões, que precisam ter suas 4 casas destruídas para perder uma vida.

Para iniciar o jogo digite 1. (Não digitar letras ou símbolos)
Logo após iniciar o jogo, digite o nome dos players.
O player1 sempre começará o jogo.
Cada jogador terá uma chance para acertar as embarcações inimigas. Caso ele atire num lugar que ja foi atingido, terá outra chance(1).

(1) = só poderá atirar numa casa já atingida SOMENTE uma vez por turno, caso atire nela duas vez o programa terá problema de funcionamento.
  
Para destruir canoas, acerte uma vez cada.
ex:	x:0 y:7


Para destruir os submarinos deve-se atirar duas vezes na posição em que você acertar. A parte do submarino será destruída e assim você poderá atirar na outra parte estando ela na esquerda, direita, em cima ou embaixo. Caso não seguir a ordem o programa poderá apresentar erro quanto à vida total das embarcações.
ex:	x:6 y:1
	x:6 y:1 (Uma parte destruída e -1 de vida)
	x:6 y:2
	x:6 y:2 (outra parte destrua) e assim por diante... 


Para destruir os porta-aviões deve-se seguir a ordem 
ex: 	x:4 y:0
	x:3 y:0
	x:2 y:0
	x:1 y:0
Isso serve para qualquer porta-aviões estando ele posicionado à direita, à esquerda ou a baixo. Caso não seguir a ordem o programa poderá apresentar erro quanto à vida total das embarcações.

O jogo acabará assim que um jogador conseguir destruir todas as embarcações de seu adversário.


Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

