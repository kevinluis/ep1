#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include <string>
#include "../inc/barcos.hpp"

using namespace std;

class PortaAvioes : public Barcos{
public:
    string get_direcao();
    void set_direcao(string direcao);
    int vida[4];
    PortaAvioes();
    ~PortaAvioes();
private:
    string direcao;

};

#endif