#ifndef PLAYERS_HPP
#define PLAYERS_HPP

#include <string>

using namespace std;

class Players{
public:
    Players();
    ~Players();
    string get_nome();
    void set_nome(string nome);
    int get_vida();
    void set_vida(int vida);
    
private:
    string nome;
    int vida;
};



#endif