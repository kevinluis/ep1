#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include <string>
#include "../inc/barcos.hpp"

using namespace std;

class Submarino : public Barcos{
public:
    string get_direcao();
    void set_direcao(string direcao);
    Submarino();
    ~Submarino();
    int vida[2];

private:    
    string direcao;

};

#endif