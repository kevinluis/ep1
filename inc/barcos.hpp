#ifndef BARCOS_HPP
#define BARCOS_HPP

#include <string>

using namespace std;

class Barcos{
    private:
 
        int coordenada_x;
        int coordenada_y;
        string embarcacao;  

    public:
        Barcos();
        ~Barcos();
        
        int get_coordenada_x();
        void set_coordenada_x(int coordenada_x);
    
        int get_coordenada_y();
        void set_coordenada_y(int coordenada_y);
    
        string get_embarcacao();
        void set_embarcacao(string embarcacao);
};

#endif