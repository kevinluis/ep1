#include <iostream>
#include <string>
#include "../inc/barcos.hpp"

using namespace std;


Barcos::Barcos(){
}
Barcos::~Barcos(){

}

int Barcos::get_coordenada_x(){
    return coordenada_x;
}
void Barcos::set_coordenada_x(int coordenada_x){
    this->coordenada_x = coordenada_x;
}
    
int Barcos::get_coordenada_y(){
    return coordenada_y;
}
void Barcos::set_coordenada_y(int coordenada_y){
    this->coordenada_y = coordenada_y;
}
    
string Barcos::get_embarcacao(){
    return embarcacao;
}
void Barcos::set_embarcacao(string embarcacao){
    this->embarcacao = embarcacao;
}