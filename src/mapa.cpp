#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include <sstream>
#include "../inc/mapa.hpp"
#include "../inc/players.hpp"
#include "../inc/barcos.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaavioes.hpp"

using namespace std;


Players player1;
Players player2;
Canoa canoa1[6]; //canoas player1
Canoa canoa2[6]; //canoas player2
Submarino submarino1[4]; //submarinos player 1
Submarino submarino2[4]; //submarinos player 2
PortaAvioes porta_aviao1[2]; //porta-avioes player 1
PortaAvioes porta_aviao2[2]; //porta-avioes player 2


void LerMapa();

void Mapa::MenuInicial(){

int opcao;

cout << " _________________________________________________ " << endl;
cout << "|                                                 |" << endl;
cout << "|                                                 |" << endl;
cout << "|                   Bem-vindo                     |" << endl;
cout << "|                                                 |" << endl;
cout << "|_________________________________________________|" << endl;
cout << endl;
cout << endl;
cout << endl;
cout << "Pressione 1 para iniciar o jogo (digite somente numeros)" << endl;
cin >> opcao;
    
    while(opcao != 1){
        cout << "Você digitou uma opção invalida" << endl;
        cout << "Digite novamente:";
        cin >> opcao;
    }

if( opcao == 1){
system("clear");
    
    string nome1;
    string nome2;

    cout << "Digite o nome do player1: ";
    cin >> nome1;
    player1.set_nome(nome1);
    player1.set_vida(16);
system("clear");
    cout << "Digite o nome do player2: ";
    cin >> nome2;
    player2.set_nome(nome2);
    player2.set_vida(16);
system("clear");
    LerMapa();
}


}

void Mapa::IniciaJogo(){
int x,y; //variaveis para coletar as coordenadas que os jogadores querem atacar
int i,j; //contadores
int a,b,k; //contadores para auxiliar na implementacao dos barcos
string comparacao; //comparar com a direcao dos barcos
char mapa1[13][13];//mapa visivel ao player 1
char mapa2[13][13];//mapa visivel ao player 2 
int vida1 = player1.get_vida();
int vida2 = player2.get_vida();// vida1 e vida2 são para controle do laço de repetição

char posicao1[13][13]; // mapa para comparação com aas coordenadas dadas pelo player 2

    for(i=0; i<13; i++){
        for(j=0; j<13; j++){
            posicao1[i][j] = '-';
        }
    }
    
    for(k=0; k<6; k++){
        a = canoa1[k].get_coordenada_x();
        b = canoa1[k].get_coordenada_y();
        posicao1[a][b] = '/';
    }
    
    for(k=0; k<4; k++){
        a = submarino1[k].get_coordenada_x();
        b = submarino1[k].get_coordenada_y();
        comparacao = submarino1[k].get_direcao();
        posicao1[a][b] = '*';
            if(comparacao == "direita"){
                posicao1[a][b+1] = '*';
            }
            else if(comparacao == "esquerda"){
                posicao1[a][b-1] = '*';
            }
            else if(comparacao == "cima"){
                posicao1[a-1][b] = '*';
            }
            else if(comparacao == "baixo"){
                posicao1[a+1][b] = '*';
            }
    }
    
    for(k=0; k<2; k++){
        a = porta_aviao1[k].get_coordenada_x();
        b = porta_aviao1[k].get_coordenada_y();
        comparacao = porta_aviao1[k].get_direcao();
        posicao1[a][b] = '+';
            if(comparacao == "esquerda"){
                for(i=1; i<=3; i++){
                    posicao1[a][b-i] = '+';
                }
            }
            else if(comparacao == "direita"){
                for(i=1; i<=3; i++){
                    posicao1[a][b+i] = '+';
                }
            }
            else if(comparacao == "cima"){
                for(i=1; i<=3; i++){
                    posicao1[a-i][b] = '+';
                }
            }
            else if(comparacao == "baixo"){
                for(i=1; i<=3; i++){
                    posicao1[a+i][b] = '+';
                }
            }
    }

char posicao2[13][13]; // mapa para comparação das coordenadas dadas pelo player 1


    for(i=0; i<13; i++){
        for(j=0; j<13; j++){
            posicao2[i][j] = '-';
        }

    }
    
    for(k=0; k<6; k++){
        a = canoa2[k].get_coordenada_x();
        b = canoa2[k].get_coordenada_y();
        posicao2[a][b] = '/';
    }
    
    for(k=0; k<4; k++){
        a = submarino2[k].get_coordenada_x();
        b = submarino2[k].get_coordenada_y();
        comparacao = submarino2[k].get_direcao();
        posicao2[a][b] = '*';
            if(comparacao == "direita"){
                posicao2[a][b+1] = '*';
            }
            else if(comparacao == "esquerda"){
                posicao2[a][b-1] = '*';
            }
            else if(comparacao == "cima"){
                posicao2[a-1][b] = '*';
            }
            else if(comparacao == "baixo"){
                posicao2[a+1][b] = '*';
            }
    }

    for(k=0; k<2; k++){
        a = porta_aviao2[k].get_coordenada_x();
        b = porta_aviao2[k].get_coordenada_y();
        comparacao = porta_aviao2[k].get_direcao();
        posicao2[a][b] = '+';
            if(comparacao == "esquerda"){
                for(i=1; i<=3; i++){
                    posicao2[a][b-i] = '+';
                }
            }
            else if(comparacao == "direita"){
                for(i=1; i<=3; i++){
                    posicao2[a][b+i] = '+';
                }
            }
            else if(comparacao == "cima"){
                for(i=1; i<=3; i++){
                    posicao2[a-i][b] = '+';
                }
            }
            else if(comparacao == "baixo"){
                for(i=1; i<=3; i++){
                    posicao2[a+i][b] = '+';
                }
            }
    }

    for(i=0; i<13; i++){
        for(j=0; j<13; j++){
            mapa1[i][j] = '-';
            mapa2[i][j] = '-';
        }
    }
int c1 = 6,c2 = 6; // contador de canoas do p1 e do p2
int vida_submarino1 = 2, vida_submarino2 = 2; // contador da vida de cada casa dos submarinos
int partes_destruidas1 = 0, partes_destruidas2 = 0; // contador de partes destruidas do porta avioes
int random_num1,random_num2; // variavel para pegar um numero randomico e determinar se o porta avioes sera atingido

while( (vida1!=0) || (vida2!=0) ){
    
    cout << "Vez do(a) " << player1.get_nome() << endl << endl;
    cout << "  00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
    for(i=0; i<13; i++){
        if(i<10){
            cout << "0" << i << " ";
        }
        else if(i>=10){
            cout << i << " ";
        }
            for(j=0; j<13; j++){
                cout << mapa1[i][j] << "  ";
            }               
        cout << endl;
    }    
    cout << endl;
    cout << "Sua vida: " <<  vida1 << endl;
    cout << "Vida do adversario: " << vida2 << endl << endl;
    cout << "Legenda: '/' = canoa quebrada||'*' = submarino destruido||'!' = submarino quebrado||'+' = parte do porta-avioes destruida";
    cout << "|| 'X' = agua" << endl;
    srand(time(0));
    random_num1 = rand() % 2;
    cout << "Escolha uma coordenada x: ";
    cin >> x;
        if(x > 12){
            cout << "Coordenada invalida" << endl;
            while(x>12){
                cout << "Escolha uma coordenada x: ";
                cin >> x;
            }
        }
    cout << endl << "Escolha uma coordenada y: ";
    cin >> y;
        if(y > 12){
            cout << "Coordenada invalida" << endl;
            while(y > 12){
                cout << "Escolha uma coordenada y: ";
                cin >> y;
            }
        }
    cout << endl;
system("clear");
        if( (mapa1[x][y] == 'X') || (mapa1[x][y] == '/') || mapa1[x][y] == '*' || mapa1[x][y] == '+'){
            cout << "Voce ja atirou nessa posicao, digite outra coordenada" << endl;
            cout << "Escolha uma coordenada x: ";
            cin >> x;
                if(x > 12){
                    while(x>12){
                        cout << "Coordenada invalida" << endl;
                        cout << "Escolha uma coordenada x: ";
                        cin >> x;
                    }
                }
            cout << endl << "Escolha uma coordenada y: ";
            cin >> y;
                if(y > 12){
                    while(y > 12){
                        cout << "Coordenada invalida" << endl;
                        cout << "Escolha uma coordenada y: ";
                        cin >> y;
                    }
                }
            cout << endl;
                if( posicao2[x][y] == '/'){
                    c2--;
                    cout << "Voce destruiu uma canoa" << endl << endl;
                    mapa1[x][y] = '/';
                    vida2--;
                }   
                else if( posicao2[x][y]== '*'){
                    if(vida_submarino2 == 2){
                        cout << "Voce acertou uma parte do submarino, acerte mais uma vez para destrui-la" << endl << endl;
                        mapa1[x][y] = '!';
                        vida_submarino2--;
                    }
                    else if(vida_submarino2 == 1){
                        cout << "Voce destruiu uma parte do submarino" << endl;
                        mapa1[x][y] = '*';
                        vida2--;
                        vida_submarino2 = 2;
                    }
                }
                else if( posicao2[x][y] == '+'){
                    if(random_num1 == 1){
                        if(partes_destruidas2 == 0){
                            cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                            partes_destruidas2++;
                            mapa1[x][y] = '+';
                        }
                        else if(partes_destruidas2 == 1){
                            cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                            partes_destruidas2++; 
                            mapa1[x][y] = '+';                   
                        }
                        else if(partes_destruidas2 == 2){
                            cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                            partes_destruidas2++;
                            mapa1[x][y] = '+';
                        }
                        else if(partes_destruidas2 == 3){
                            cout << "Voce destruiu um porta-avioes" << endl;
                            vida2--;
                            mapa1[x][y] = '+';
                            partes_destruidas2 = 0;
                        }
                    }
                    else{
                        cout << "O seu missil foi abatido pelo porta-avioes" <<  endl << endl;
                    }
                }
                else if( posicao2[x][y] == '-'){
                    cout << "Voce errou, tente na proxima rodada" << endl << endl;
                    mapa1[x][y] = 'X';
                }
    
            cout << "  00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
                for(i=0; i<13; i++){
                    if(i<10){
                        cout << "0" << i << " ";
                    }
                    else if(i>=10){
                    cout << i << " ";
                    }
                        for(j=0; j<13; j++){
                            cout << mapa1[i][j] << "  ";
                        }               
                    cout << endl;
                }
            sleep(5);
            system("clear");            
        }
        else{
            if( posicao2[x][y] == '/'){
                c2--;
                cout << "Voce destruiu uma canoa" << endl << endl;
                mapa1[x][y] = '/';
                vida2--;
            }   
            else if( posicao2[x][y]== '*'){
                if(vida_submarino2 == 2){
                    cout << "Voce acertou uma parte do submarino, acerte mais uma vez para destrui-la" << endl << endl;
                    mapa1[x][y] = '!';
                    vida_submarino2--;
                }
                else if(vida_submarino2 == 1){
                    cout << "Voce destruiu uma parte do submarino" << endl;
                    mapa1[x][y] = '*';
                    vida2--;
                    vida_submarino2 = 2;
                }
            }
            else if( posicao2[x][y] == '+'){
                if(random_num1 == 1){
                    if(partes_destruidas2 == 0){
                        cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                        partes_destruidas2++;
                        mapa1[x][y] = '+';
                    }
                    else if(partes_destruidas2 == 1){
                        cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                        partes_destruidas2++; 
                        mapa1[x][y] = '+';                   
                    }
                    else if(partes_destruidas2 == 2){
                        cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                        partes_destruidas2++;
                        mapa1[x][y] = '+';
                    }
                    else if(partes_destruidas2 == 3){
                        cout << "Voce destruiu um porta-avioes" << endl;
                        vida2--;
                        mapa1[x][y] = '+';
                        partes_destruidas2 = 0;
                    }
                }
                else{
                    cout << "O seu missil foi abatido pelo porta-avioes" <<  endl << endl;
                }
            }
            else if( posicao2[x][y] == '-'){
                cout << "Voce errou, tente na proxima rodada" << endl << endl;
                mapa1[x][y] = 'X';
            }
    
            cout << "  00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
                for(i=0; i<13; i++){
                    if(i<10){
                        cout << "0" << i << " ";
                    }
                    else if(i>=10){
                    cout << i << " ";
                    }
                        for(j=0; j<13; j++){
                            cout << mapa1[i][j] << "  ";
                        }               
                    cout << endl;
                }
            sleep(5);
            system("clear");    
        }
    cout << "Vez do(a) " << player2.get_nome() << endl << endl;
    cout << "  00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
    for(i=0; i<13; i++){
        if(i<10){
            cout << "0" << i << " ";
        }
        else if(i>=10){
            cout << i << " ";
        }
            for(j=0; j<13; j++){
                cout << mapa2[i][j] << "  ";
            }               
        cout << endl;
    }    
    cout << endl;
    cout << "Sua vida: " <<  vida2 << endl;
    cout << "Vida do adversario: " << vida1 << endl << endl;
    cout << "Legenda: '/' = canoa quebrada||'*' = submarino destruido||'!' = submarino quebrado||'+' = parte do porta-avioes destruida";
    cout << "|| 'X' = agua" << endl;
    srand(time(0));
    random_num2 = rand() % 2;
    cout << "Escolha uma coordenada x: ";
    cin >> x;
        if(x > 12){
            cout << "Coordenada invalida" << endl;
            while(x>12){
                cout << "Escolha uma coordenada x: ";
                cin >> x;
            }
        }
    cout << endl << "Escolha uma coordenada y: ";
    cin >> y;
        if(y > 12){
            cout << "Coordenada invalida" << endl;
            while(y > 12){
                cout << "Escolha uma coordenada y: ";
                cin >> y;
            }
        }
    cout << endl;
system("clear");
        if( (mapa2[x][y] == 'X') || (mapa2[x][y] == '/') || mapa2[x][y] == '*' || mapa2[x][y] == '+'){
            cout << "Voce ja atirou nessa posicao, digite outra coordenada" << endl;
            cout << "Escolha uma coordenada x: ";
            cin >> x;
                if(x > 12){
                    while(x>12){
                        cout << "Coordenada invalida" << endl;
                        cout << "Escolha uma coordenada x: ";
                        cin >> x;
                    }
                }
            cout << endl << "Escolha uma coordenada y: ";
            cin >> y;
                if(y > 12){
                    while(y > 12){
                        cout << "Coordenada invalida" << endl;
                        cout << "Escolha uma coordenada y: ";
                        cin >> y;
                    }
                }
            cout << endl;
                if( posicao1[x][y] == '/'){
                    c1--;
                    cout << "Voce destruiu uma canoa" << endl << endl;
                    mapa2[x][y] = '/';
                    vida1--;
                }   
                else if( posicao1[x][y]== '*'){
                    if(vida_submarino1 == 2){
                        cout << "Voce acertou uma parte do submarino, acerte mais uma vez para destrui-la" << endl << endl;
                        mapa2[x][y] = '!';
                        vida_submarino1--;
                    }
                    else if(vida_submarino1 == 1){
                        cout << "Voce destruiu uma parte do submarino" << endl;
                        mapa2[x][y] = '*';
                        vida1--;
                        vida_submarino1 = 2;
                    }
                }
                else if( posicao1[x][y] == '+'){
                    if(random_num2 == 1){    
                        if(partes_destruidas1 == 0){
                            cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                            partes_destruidas1++;
                            mapa2[x][y] = '+';
                        }
                        else if(partes_destruidas1 == 1){
                            cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                            partes_destruidas1++; 
                            mapa2[x][y] = '+';                   
                        }
                        else if(partes_destruidas1 == 2){
                            cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                            partes_destruidas1++;
                            mapa2[x][y] = '+';
                        }
                        else if(partes_destruidas1 == 3){
                            cout << "Voce destruiu um porta-avioes" << endl;
                            vida1--;
                            mapa2[x][y] = '+';
                            partes_destruidas1 = 0;
                        }
                    }
                    else{
                        cout << "O seu missil foi abatido pelo porta avioes" << endl << endl;
                    }
                }
                else if( posicao1[x][y] == '-'){
                    cout << "Voce errou, tente na proxima rodada" << endl << endl;
                    mapa2[x][y] = 'X';
                }
    
            cout << "  00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
                for(i=0; i<13; i++){
                    if(i<10){
                        cout << "0" << i << " ";
                    }
                    else if(i>=10){
                    cout << i << " ";
                    }
                        for(j=0; j<13; j++){
                            cout << mapa2[i][j] << "  ";
                        }               
                    cout << endl;
                }
            sleep(5);
            system("clear");            
        }
        else{
            if( posicao1[x][y] == '/'){
                c1--;
                cout << "Voce destruiu uma canoa" << endl << endl;
                mapa2[x][y] = '/';
                vida1--;
            }   
            else if( posicao1[x][y]== '*'){
                if(vida_submarino1 == 2){
                    cout << "Voce acertou uma parte do submarino, acerte mais uma vez para destrui-la" << endl << endl;
                    mapa2[x][y] = '!';
                    vida_submarino1--;
                }
                else if(vida_submarino1 == 1){
                    cout << "Voce destruiu uma parte do submarino" << endl;
                    mapa2[x][y] = '*';
                    vida1--;
                    vida_submarino1 = 2;
                }
            }
            else if( posicao1[x][y] == '+'){
                if(random_num2 == 1){
                    if(partes_destruidas1 == 0){
                        cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                        partes_destruidas1++;
                        mapa2[x][y] = '+';
                    }
                    else if(partes_destruidas1 == 1){
                        cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                        partes_destruidas1++; 
                        mapa2[x][y] = '+';                   
                    }
                    else if(partes_destruidas1 == 2){
                        cout << "Voce destruiu uma parte do porta-avioes" << endl << endl;
                        partes_destruidas1++;
                        mapa2[x][y] = '+';
                    }
                    else if(partes_destruidas1 == 3){
                        cout << "Voce destruiu um porta-avioes" << endl;
                        vida1--;
                        mapa2[x][y] = '+';
                        partes_destruidas1 = 0;
                    }
                }
                else{
                    cout << "O seu missil foi abatido pelo porta-avioes" <<  endl << endl;
                }
            }
            else if( posicao1[x][y] == '-'){
                cout << "Voce errou, tente na proxima rodada" << endl << endl;
                mapa2[x][y] = 'X';
            }
    
            cout << "  00 01 02 03 04 05 06 07 08 09 10 11 12" << endl;
                for(i=0; i<13; i++){
                    if(i<10){
                        cout << "0" << i << " ";
                    }
                    else if(i>=10){
                    cout << i << " ";
                    }
                        for(j=0; j<13; j++){
                            cout << mapa2[i][j] << "  ";
                        }               
                    cout << endl;
                }
            sleep(5);
            system("clear");    
        }


if( (vida1==0) || (vida2==0) ){
    break;
}
}
if(vida1 == 0){
    cout << "O " << player2.get_nome() << " ganhou o jogo" << endl;
}
else if(vida2 == 0){
    cout << "O " << player1.get_nome() << " ganhou o jogo" << endl;
}
}

void LerMapa(){

    int i = 0; //variavel para controlar a ordenacao das canoas
    int j = 0; //variavel para controlar a ordenacao dos submarinos
    int k = 0; //variavel para controlar a ordenacao dos porta-avioes
    int x;
    int y;
    string tipo;
    string orientacao;

    ifstream mapa;
    string linha;

    mapa.open("doc/mapa.txt");


    if(mapa.is_open()){
        while( !mapa.eof() ){
        getline(mapa, linha);
            if ( linha[0] == '#'||linha == "" ){
                continue;
            }
            else {
                stringstream ss;
                ss << linha;
                ss >> x;
                ss >> y;
                ss >> tipo;
                ss >> orientacao;

                    if (tipo == "canoa"){
                        if(i < 6){
                            
                            canoa1[i].set_coordenada_x(x);
                            canoa1[i].set_coordenada_y(y);
                            
                        }
                        else if(i >= 6 && i < 12){
                            
                            canoa2[i-6].set_coordenada_x(x);
                            canoa2[i-6].set_coordenada_y(y);
                        
                        }
                        
                        i++;
                    }
                    else if(tipo == "submarino"){
                        if(j < 4){
                            
                            submarino1[j].set_coordenada_x(x);
                            submarino1[j].set_coordenada_y(y);
                            submarino1[j].set_direcao(orientacao);

                        }
                        else if(j>=4 && j<8){
                            
                            submarino2[j-4].set_coordenada_x(x);
                            submarino2[j-4].set_coordenada_y(y);
                            submarino2[j-4].set_direcao(orientacao);
                        }
                        j++;
                    }
                    else if(tipo == "porta-avioes"){
                        if(k < 2){

                            porta_aviao1[k].set_coordenada_x(x);
                            porta_aviao1[k].set_coordenada_y(y);
                            porta_aviao1[k].set_direcao(orientacao);
                            
                        }
                        else if(k>=2 && k<4){

                            porta_aviao2[k-2].set_coordenada_x(x);
                            porta_aviao2[k-2].set_coordenada_y(y);
                            porta_aviao2[k-2].set_direcao(orientacao);
                           
                        }
                        k++;
                    }                              
            }

        }
    }
    else{
        cout << "Nao foi possivel abrir o mapa" << endl;
    }

}
