#include <iostream>
#include <string>
#include <fstream>
#include "../inc/mapa.hpp"
#include "../inc/players.hpp"
#include "../inc/barcos.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaavioes.hpp"

using namespace std;

int main(){
    Mapa mapa;
    mapa.MenuInicial();
    mapa.IniciaJogo();

    return 0;
}



